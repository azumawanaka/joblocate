<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index');
// Route::post('/search', 'HomeController@search')->name("search");
Route::view('/unauthorized', 'unauthorized');

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::put('/profile/update/{id}', 'ProfileController@update')->name('profile.update');

Route::get('/jobs', 'JobsController@index')->name('jobs');
Route::get('/jobs/new', 'JobsController@create');
Route::post('/jobs', 'JobsController@store');
Route::get('/jobs/{id}', array('as' => 'jobs.show', 'uses' => 'JobsController@show'));
Route::get('/jobs/destroy/{id}', array('as' => 'jobs.destroy', 'uses' => 'JobsController@destroy'));
Route::get('/jobs/edit/{id}', array('as' => 'jobs.edit', 'uses' => 'JobsController@edit'));
Route::post('/jobs/update/{id}', array('as' => 'jobs.update', 'uses' => 'JobsController@update'));
Route::post('/jobs/apply/{id}', array('as' => 'jobs.apply', 'uses' => 'JobsController@apply'));
Route::put('/jobs/hire/{id}',array('as' => 'jobs.hireuser', 'uses' => 'JobsController@hire'))->name("jobs");
Route::put('/jobs/decline/{id}',array('as' => 'jobs.declineuser', 'uses' => 'JobsController@decline'))->name("jobs");

Route::get('/applications', 'ApplicationsController@index')->name('applications');

Route::get('emailNotify', 'JobsController@emailTest');

// Route::group(['middleware' => 'App\Http\Middleware\ClientMiddleware'], function()
// {
//     Route::match(['get','post'], '/jobs/new', 'HomeController@create');
    
// });

Route::group(['middleware' => 'App\Http\Middleware\FreelancerMiddleware'], function()
{
    Route::match(['get','post'], '/jobs/new', 'JobsController@create');
    Route::match(['get'], '/jobs', 'JobsController@index')->name('jobs');
});