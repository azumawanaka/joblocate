@extends('layouts.app')
@section('title')
    @if (!$myapplications)
    Jobs - {{ ucfirst($post_info[0]->title) }}
    @else
    My Applications
    @endif
@endsection
@section('content')
    <div class="content">
        @if (Auth::check())
        <div class="container">
            <div class="row">
                @if ($myapplications)
                    <h2 class="mb-4">My Applications</h2>
                    @foreach ($applications as $app)
                        <div class="col-md-12 col-xs-12 mb-4 card p-4">
                            <h2 class="border-bottom pb-2">
                                <span class="badge badge-info" style="font-size: 12px;">{{ ucfirst($app->setas) }}</span> {{ ucfirst($app->title) }}
                            </h2>
                            <small class="text-muted">
                                Date Of Application: <span class="text-info">{{$app->created_at}}</span>
                            </small>
                            <small class="text-muted d-block">
                                Status: 
                                @if ($app->status == 1)
                                    <strong class="badge badge-success">Hired</strong>
                                @elseif($app->status == 2)
                                    <strong class="badge badge-danger">Declined</strong>
                                @else 
                                    <strong class="badge badge-primary">Pending</strong>
                                @endif
                            </small>
                            <div class="descriptions mb-4 mt-3">
                                <strong>Job Descriptions:</strong> <br>{{ $app->description }}
                            </div>
                            <div class="skills mb-3">
                                <h5>Required skill/s needed: </h5>
                                <?php
                                    $category = json_decode($app->categories);
                                    $cat_total = count($category);
                                    $cat_counter = 0;
                                    if(isset($app->custom_categories) && !empty($app->custom_categories)) :
                                    $custom_category = json_decode($app->custom_categories);
                                    ?>
                                    @foreach ($custom_category as $ccat)
                                        <?php
                                            array_push($category, ', '.$ccat);
                                        ?>
                                    @endforeach
                                @endif
                                <h6 class="text-info">
                                @foreach ($category as $cat)
                                    <?php $cat_counter++ ?>
                                    {{ $cat }}
                                    @if ($cat_total > $cat_counter)
                                        ,
                                    @endif
                                @endforeach
                                </h6>
                            </div>
                            <h6 class="text-muted">Hourly Rate: <span class="text-primary">{{ $app->rate }}</span></h6>
                        </div>
                    @endforeach
                    {{-- paginate --}}
                    <div class="p-3">
                    <?php echo $applications->render(); ?>
                    </div>
                    {{-- //paginate --}}
                @else
                    @if (auth()->user()->roles !="freelancer")
                    <div class="col-md-9 col-xs-12">
                    @else 
                    <div class="col-md-12 col-xs-12">
                    @endif
                        <div class="row">
                            <div class="col-md-12 p-0">
                                <h2 class="border-bottom pb-2"><span class="badge badge-info" style="font-size: 12px;">{{ ucfirst($post_info[0]->setas) }}</span> {{ ucfirst($post_info[0]->title) }}</h2>
                                <div class="descriptions mb-4">
                                    {{ $post_info[0]->description }}
                                </div>
                                <div class="skills mb-5">
                                    <h5>Required skill/s needed: </h5>
                                    <?php
                                        $category = json_decode($post_info[0]->categories);
                                        $cat_total = count($category);
                                        $cat_counter = 0;
                                        if(isset($post_info[0]->custom_categories) && !empty($post_info[0]->custom_categories)) :
                                        $custom_category = json_decode($post_info[0]->custom_categories);
                                        ?>
                                        @foreach ($custom_category as $ccat)
                                            <?php
                                                array_push($category, ', '.$ccat);
                                            ?>
                                        @endforeach
                                    @endif
                                    <h6 class="text-info mb-5">
                                    @foreach ($category as $cat)
                                        <?php $cat_counter++ ?>
                                        {{ $cat }}
                                        @if ($cat_total > $cat_counter)
                                            ,
                                        @endif
                                    @endforeach
                                    </h6>
                                    <h4 class="mb-4">Applicants <small class="text-info">({{count($applicants)}})</small></h4>
                                    @if (auth()->user()->roles != "freelancer" && auth()->user()->id == $post_info[0]->userid)
                                        <div class="list-group">
                                            @foreach ($applicants as $item)
                                                <a class="list-group-item d-flex justify-content-between" href="#aboutModal" data-toggle="modal" data-target="#myModal_{{$item->uid}}">
                                                    <div class="d-flex">
                                                        <span style="display:inline-block; width: 25px; height: 25px;overflow:hidden;" class="mr-2">
                                                            <img style="width: 100%;" src="{{ URL::asset('uploads/avatar') }}/{{ $item->avatar }}" alt="">  {{$item->name}} 
                                                        </span>
                                                        <small class="text-muted">({{$item->email}})</small>
                                                    </div>
                                                    <?php
                                                        $status = "";
                                                    ?>
                                                    @if ($item->appStatus == 1)
                                                        <small class="text-success float-right">Hired <i class="fa fa-check"></i></small>
                                                    @endif    
                                                    @if($item->appStatus == 2)
                                                        <small class="text-muted float-right">Declined <i class="fa fa-times"></i></small>
                                                    @endif
                                                </a>
                                                
                                                <!-- Modal -->
                                                <div class="modal fade" id="myModal_{{$item->uid}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4>
                                                                    @if ($item->appStatus == 1)
                                                                        <span class="text-success">Hired</span>
                                                                    @endif    
                                                                    @if($item->appStatus == 2)
                                                                        <span class="text-muted">Declined</span>
                                                                    @endif
                                                                </h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                    <i class="fa fa-close"></i>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center mb-4">
                                                                    <div style="overflow:hidden; width: 250px; height: 250px;margin: auto;">
                                                                        <img src="{{ URL::asset('uploads/avatar') }}/{{ $item->avatar }}" style="width: 100%;" border="0" class="img-circle">
                                                                    </div>
                                                                    <h4 class="media-heading mb-0 pt-2">{{$item->name}}</h4>
                                                                    <small class="media-heading">{{$item->email}}</small>
                                                                </div>
                                                                <hr>
                                                                <small class="text-info"><strong>Application Date:</strong> {{$item->created_at}}</small>

                                                                @if ($item->appStatus == 1)
                                                                    <small class="d-block text-success"><strong>Date Hired:</strong> {{$item->hired_at}}</small>
                                                                @endif    
                                                                @if($item->appStatus == 2)
                                                                    <small class="d-block text-danger"><strong>Date Declined:</strong> {{$item->declined_at}}</small>
                                                                @endif
                                                                <div class="d-block mt-3"><strong>Skills: </strong>
                                                                    <p>
                                                                    <?php
                                                                        $skills = json_decode($item->skills);
                                                                        $cat_total = count($skills);
                                                                        $cat_counter = 0;
                                                                        if(isset($item->other_skills) && !empty($item->other_skills)) :
                                                                        $other_skills = json_decode($item->other_skills);
                                                                    ?>
                                                                        @foreach ($other_skills as $ccat)
                                                                            <?php
                                                                                array_push($skills, ', '.$ccat);
                                                                            ?>
                                                                        @endforeach
                                                                    @endif

                                                                    @foreach ($skills as $cat)
                                                                        <?php $cat_counter++ ?>
                                                                        <span>{{ $cat }}</span>
                                                                        @if ($cat_total > $cat_counter)
                                                                            ,
                                                                        @endif
                                                                    @endforeach
                                                                    </p>
                                                                </div>
                                                                <p class="text-left mt-2"><strong>Bio: </strong>
                                                                    {{$item->publicInfo ? $item->publicInfo:"N/A"}}
                                                                </p>
                                                                <p class="text-left mt-2"><strong>CV: </strong><br>
                                                                    {!! $item->cv ? '<i class="fa fa-file"></i> ':"N/A" !!}<a href="{{ URL::asset('uploads/cv') }}/{{ $item->cv }}" target="_blank">{{ $item->cv }}</a>
                                                                </p>
                                                                <hr>
                                                                <div class="row d-flex justify-content-between">
                                                                    @if ($item->appStatus==1)
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <a href="#" class="btn btn-info btn-block btn-md">Schedule a Meeting</a>
                                                                    </div>
                                                                    @else
                                                                    <form action="{{ route("jobs") }}/hire/{{encrypt($item->appID)}}" method="POST" class="col-md-4 col-xs-12 mb-2">
                                                                        @csrf
                                                                        {{ method_field('PUT') }}
                                                                        <input type="submit" class="btn btn-success btn-md btn-block" value="Hire Me">
                                                                    </form> 
                                                                    <form action="{{ route("jobs") }}/decline/{{encrypt($item->appID)}}" method="POST" class="col-md-4 col-xs-12 mb-2">
                                                                        @csrf
                                                                        {{ method_field('PUT') }}
                                                                        <input type="submit" class="btn btn-secondary btn-md btn-block" value="Decline">
                                                                    </form> 
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                    @endif
                                </div>
                                    @if (auth()->user()->roles == "freelancer")
                                        @if ($appStatus)
                                            <div class="bg-light d-block d-flex justify-content-between p-3">
                                                <span><i class="fa fa-check text-success"></i> You already applied to this job post.</span>
                                                <small class="text-muted">{{ date("d M, Y H:i", strtotime($post_info[0]->created_at))}}</small>
                                            </div>
                                        @else
                                            <form action="{{ route("jobs") }}/apply/{{encrypt($post_info[0]->jID)}}" method="POST">
                                                @csrf
                                                <input type="submit" class="btn btn-primary btn-md" value="Apply to this job">
                                            </form>   
                                        @endif  
                                    @endif
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @else 
            <h1 class="title text-center m-b-md">
                Welcome Visitor!
            </h1>
        @endif
        
    </div>
@endsection
