@extends('layouts.app')
@section('title')
All Job Posts
@endsection
@section('content')
    <div class="content">
        @if (Auth::check())
        <div class="container">
            <div class="row">
                
                @if (auth()->user()->roles !="freelancer")
                <div class="col-md-3 ">
                    @include('layouts/sidebar') 
                </div>
                @endif
                @if (auth()->user()->roles !="freelancer")
                <div class="col-md-9 col-xs-12">
                @else 
                <div class="col-md-12 col-xs-12 p-0">
                @endif
                    <div class="card border-0">
                        <div class="card-body">
                            @if (auth()->user()->roles !="freelancer")
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>All Job Posts</h4>
                                    <hr>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- view all jobs --}}
                                    <ul class="list-group p-3">
                                        @foreach ($jobs as $item)


                                            <li class="list-group-item">
                                                <h3 class="title border-bottom">
                                                    {{ ucfirst($item->title) }}
                                                </h3>
                                                <div class="category text-primary mb-3">
                                                    <strong class="d-block text-info"> <i class="fa fa-money"></i> {{ $item->rate }}</strong>
                                                    <i class="fa fa-tag"></i>
                                                    <?php
                                                    $category = json_decode($item->categories);
                                                    $cat_total = count($category);
                                                    $cat_counter = 0;
                                                    if(isset($item->custom_categories) && !empty($item->custom_categories)) :
                                                    $custom_category = json_decode($item->custom_categories);
                                                    ?>
                                                    @foreach ($custom_category as $ccat)
                                                        <?php
                                                            array_push($category, ', '.$ccat);
                                                        ?>
                                                    @endforeach
                                                @endif

                                                @foreach ($category as $cat)
                                                    <?php $cat_counter++ ?>
                                                    {{ $cat }}
                                                    @if ($cat_total > $cat_counter)
                                                        ,
                                                    @endif
                                                @endforeach
                                                </div>
                                                <div class="descriptions mb-3">
                                                    {{ mb_strimwidth($item->description, 0, 150, ".....") }}
                                                </div>
                                                <div class="bids text-danger">
                                                    {{ $item->applicationsCount }} applied
                                                </div>
                                                <div class="bids mt-3 text-muted">
                                                    <img width="20" src="{{ URL::asset('img') }}/{{Auth::user()->avatar}}" alt=""> 
                                                    @if ($item->name == auth()->user()->name)
                                                        You
                                                    @else 
                                                        @if (isset($item->publicName) && !empty($item->publicName))
                                                            {{$item->publicName}}
                                                        @else 
                                                            {{$item->name}}
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="dates mt-2 text-right">
                                                <small class="posted text-muted"> <i class="fa fa-calendar"></i> {{$item->updated_at}}</small>
                                                </div>
                                                <div class="group">
                                                    <a href="{{ route("jobs") }}/{{encrypt($item->id)}}" class="btn btn-primary btn-sm text-left">
                                                        Read more..
                                                    </a>
                                                </div>
                                            </li>

                                        @endforeach
                                    </ul>
                                    {{-- all jobs --}}
                                    {{-- paginate --}}
                                    <div class="p-3">
                                        <?php echo $jobs->render(); ?>
                                    </div>
                                    {{-- //paginate --}}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else 
            <h1 class="title text-center m-b-md">
                Welcome Visitor!
            </h1>
        @endif
        
    </div>
@endsection