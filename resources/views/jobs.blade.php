@extends('layouts.app')
@section('title')
    @if (!empty($title))
        {{$title}}
    @else 
        Jobs
    @endif
@endsection
@section('content')
    <div class="content">
        @if (Auth::check())
        <div class="container">
                <div class="row">
                    <div class="col-md-3 ">
                        @include('layouts/sidebar') 
                    </div>
                    <div class="col-md-9">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>
                                        @if (isset($title) && !empty($title))
                                            {{$title}}
                                        @endif
                                    </h4>
                                    <hr>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    @if ($create)
                                        @if ( isset($id) && !empty($id) ) 
                                        <form action="{{ route("jobs") }}/update/{{encrypt($id)}}" method="POST" class="form new-job" novalidate>
                                        @else 
                                        <form action="{{ route("jobs") }}" method="POST" class="form new-job" novalidate>
                                        @endif
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <label for="title">Title <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="title" value="@if (isset($jobs[0]->title) && !empty($jobs[0]->title)) {{$jobs[0]->title}} @endif" minlength="4" name="title" placeholder="Need Wordpress Developer" required>
                                                <div class="invalid-feedback">Please add some title.</div>
                                            </div>
                                            <div class="form-group">
                                                <label for="description">Description <span class="text-danger">*</span></label>
                                                <textarea name="description" id="description" rows="5" class="form-control" placeholder="Enter job's info here.." required>@if (isset($jobs[0]->description) && !empty($jobs[0]->description)) {{$jobs[0]->description}} @endif</textarea>
                                                <div class="invalid-feedback">Please add some descriptions.</div>
                                            </div>
                                            <div class="form-group">
                                                <label for="category" class="col-md-12 col-xs-12 row">Categories</label>
                                                @foreach (json_decode($skills[0]->skill) as $item)
                                                    <div class="custom-control custom-checkbox custom-control-inline mb-2">
                                                        <input type="checkbox" class="custom-control-input" name="category[]" @if (isset($jobs[0]->categories) && !empty($jobs[0]->categories)) @if (in_array($item, json_decode($jobs[0]->categories))) checked  @endif @endif id="{{$item}}" value="{{$item}}">
                                                        <label class="custom-control-label" for="{{$item}}">{{ucfirst($item)}}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="form-group">
                                                <label for="more">Custom Category <small class="text-danger">(1 item per enter)</small></label>
                                                <textarea name="more_category" class="form-control" id="more_category" cols="30" rows="4">@if (isset($jobs[0]->custom_categories) && !empty($jobs[0]->custom_categories))<?php
                                                        $custom_categories = json_decode($jobs[0]->custom_categories);
                                                        $cc_total = count($custom_categories);
                                                        $cc_counter = 0;
                                                        foreach ($custom_categories as $key => $value) { $cc_counter++;
                                                            echo $value;
                                                            if( $cc_counter < $cc_total ) {
                                                                echo "\r";
                                                            }
                                                        } ?>@endif</textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="setas">Set As</label><br>
                                                <div class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="radio" class="custom-control-input" name="setas" @if (isset($jobs[0]->setas) && !empty($jobs[0]->setas)) @if ($jobs[0]->setas == "parttime") checked  @endif @endif id="parttime" value="parttime" checked>
                                                    <label class="custom-control-label" for="parttime">Part Time</label>
                                                </div>
                                                <div class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="radio" class="custom-control-input" name="setas" @if (isset($jobs[0]->setas) && !empty($jobs[0]->setas)) @if ($jobs[0]->setas == "fulltime") checked  @endif @endif id="fulltime" value="fulltime">
                                                    <label class="custom-control-label" for="fulltime">Full Time</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="max_bids">Maximum Applicants <span class="text-danger">*</span></label>
                                                <input type="number" min="1" class="form-control" id="max_bids" name="max_bids" value="@if(isset($jobs[0]->max_bids) && !empty($jobs[0]->max_bids)){{$jobs[0]->max_bids}}@endif" required>
                                                <div class="invalid-feedback">Please add maximum applicants.</div>
                                            </div>
                                            <div class="form-group">
                                                <label for="rate">Job/Task Rate <span class="text-danger">*</span></label>
                                                <input type="text" name="rate" id="rate" class="form-control" value="@if (isset($jobs[0]->rate) && !empty($jobs[0]->rate)) {{ $jobs[0]->rate }} @endif" required>
                                                <div class="invalid-feedback">Please add job's rate.</div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exp_date">Expiry Date <span class="text-danger">*</span></label>
                                                <input type="text" name="exp_date" id="exp_date" class="form-control" value="@if (isset($jobs[0]->max_bids) && !empty($jobs[0]->max_bids)) {{ $jobs[0]->expiry_date }} @endif" required>
                                                <div class="invalid-feedback">Please add job's expiry date.</div>
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Status <span class="text-danger">*</span></label><br>
                                                <div class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="radio" class="custom-control-input" name="status" @if (isset($jobs[0]->status) && !empty($jobs[0]->status)) @if ($jobs[0]->status == "active") checked  @endif @endif id="active" value="active" checked>
                                                    <label class="custom-control-label" for="active">Active</label>
                                                </div>
                                                <div class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="radio" class="custom-control-input" name="status" @if (isset($jobs[0]->status) && !empty($jobs[0]->status)) @if ($jobs[0]->status == "inactive") checked  @endif @endif id="inactive" value="inactive">
                                                    <label class="custom-control-label" for="inactive">Inactive</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success btn-md btn-block">Save</button>
                                            </div>
                                        </form>

                                    @else 
                                        {{-- view all jobs --}}
                                        <ul class="list-group p-3">
                                            @foreach ($jobs as $item)
                                            
                                            @if ($item->userid == auth()->user()->id)
                                                
                                            <li class="list-group-item">
                                                <div class="text-right">
                                                    <small>
                                                        <a href="{{ route("jobs") }}/destroy/{{encrypt($item->id)}}" class="text-danger" data-confirm="true" data-confirm-message="Continue delete this item?"><i class="fa fa-trash"></i> Trash</a> 
                                                        | <a href="{{ route("jobs") }}/edit/{{encrypt($item->id)}}" class="text-primary"><i class="fa fa-pencil"></i> Edit</a>
                                                    </small>
                                                </div>
                                                <a href="{{ route("jobs") }}/{{encrypt($item->id)}}" class="btn btn-sm text-left d-block">
                                                    <h3 class="title border-bottom">
                                                        {{ ucfirst($item->title) }}
                                                    </h3>
                                                    <div class="category text-primary mb-3">
                                                        <strong class="d-block text-info"> <i class="fa fa-money"></i> {{ $item->rate }}</strong>
                                                        <i class="fa fa-tag"></i>
                                                        <?php
                                                            $category = json_decode($item->categories);
                                                            $cat_total = count($category);
                                                            $cat_counter = 0;
                                                            if(isset($item->custom_categories) && !empty($item->custom_categories)) :
                                                            $custom_category = json_decode($item->custom_categories);
                                                        ?>
                                                            @foreach ($custom_category as $ccat)
                                                                <?php
                                                                    array_push($category, ', '.$ccat);
                                                                ?>
                                                            @endforeach
                                                        @endif

                                                        @foreach ($category as $cat)
                                                            <?php $cat_counter++ ?>
                                                            {{ $cat }}
                                                            @if ($cat_total > $cat_counter)
                                                                ,
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <div class="descriptions mb-3">
                                                        {{ mb_strimwidth($item->description, 0, 150, ".....") }}
                                                    </div>
                                                    <div class="bids text-success">
                                                        <i class="fa fa-user"> </i> {{ $item->applicationsCount }} applied
                                                    </div>
                                                    <div class="dates mt-2 text-right">
                                                        <small class="posted text-muted"> <i class="fa fa-calendar"></i> {{$item->updated_at}}</small>
                                                    </div>
                                                </a>
                                            </li>

                                            @endif
                                                
                                            @endforeach
                                        </ul>
                                        {{-- all jobs --}}
                                        {{-- paginate --}}
                                        <div class="p-3">
                                            <?php echo $jobs->render(); ?>
                                        </div>
                                        {{-- //paginate --}}
                                    @endif
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        @else 
            <h1 class="title text-center m-b-md">
                Welcome Visitor!
            </h1>
        @endif
        
    </div>
@endsection