<div class="list-group ">
    <a href="#" class="list-group-item list-group-item-action dropdown-toggle dtoggle-v2 {{ Request::path() == 'jobs' || Request::path() == 'jobs/new'  ? 'active' : '' }}" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> 
        <i class="fa fa-suitcase text-primary"></i> My Job Posts
    </a>
    <div class="dropdown-menu">
    <a class="dropdown-item" href="{{ route("jobs") }}">All</a>
        <a class="dropdown-item" href="{{ route("jobs") }}/new">Add New</a>
    </div>
    <a href="#" class="list-group-item list-group-item-action">Enquiry</a>
    <a href="#" class="list-group-item list-group-item-action">Dealer</a>
    <a href="#" class="list-group-item list-group-item-action">Media</a>
    <a href="#" class="list-group-item list-group-item-action">Post</a>
    <a href="#" class="list-group-item list-group-item-action">Category</a>
    <a href="#" class="list-group-item list-group-item-action">New</a>
    <a href="#" class="list-group-item list-group-item-action">Comments</a>
    <a href="#" class="list-group-item list-group-item-action">Appearance</a>
    <a href="#" class="list-group-item list-group-item-action">Reports</a>
    <a href="#" class="list-group-item list-group-item-action">Settings</a>
</div>