<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'JobLocate') }} - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    
    <!-- Bootstrap core CSS -->
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />    
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">
    
    
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'JobLocate') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
    
                <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                    <form class="form-inline my-2 my-lg-0" action="{{ url("/") }} " method="GET">
                        <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search job.." aria-label="Search">
                    </form>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav float-right">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link btn btn-sm" href="{{ route('login') }}"> <i class="fa fa-sign-in"></i> {{ __('Login') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <?php $file_exist = 'uploads/avatar'."/".Auth::user()->avatar; ?>
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <img style="border-radius: 4px;width: 25px;height:25px;" src="<?php if(file_exists($file_exist) ){ echo URL::asset('/').$file_exist; }else{ echo URL::asset('img').'/'.Auth::user()->avatar; } ?>" alt=""> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <ul class="nav flex-column">
                                        <li>
                                            <a class="dropdown-item" href="{{ route('profile') }}">
                                            {{ __('My Profile') }}
                                            </a>
                                        </li>
                                        @if (auth()->user()->roles == "freelancer")
                                        <li>
                                            <a class="dropdown-item" href="{{ route('applications') }}">
                                            {{ __('Applications') }}
                                            </a>
                                        </li>
                                        @endif
                                        <li>
                                            <a class="dropdown-item" href="{{ url('/') }}">
                                            {{ __('All Jobs') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                            </a>
        
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            {{-- alerts --}}
            <div class="container" id="alert-cntr">
                @if (isset(Session::get('message')['type']))

                    @component('alert')
                        @slot('type')
                            {{Session::get('message')['type']}}
                        @endslot

                        @slot('title')
                            {{Session::get('message')['title']}}
                        @endslot
                        {{Session::get('message')['msg']}}
                    @endcomponent
                    
                @endif
                
            </div>
            {{-- //alerts --}}
            @yield('content')
        </main>
    </div>
</body>
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ URL::asset('js/main.js') }}"></script>
    <script>
        $(function(){var $j = jQuery.noConflict();
            $('#exp_date').datepicker();
        });
    </script>
</html>
