@extends('layouts.app')
@section('title')
    Profile
@endsection
@section('content')
    <div class="content">
        @if (Auth::check())
            <div class="container">
                <div class="row">
                    @if (auth()->user()->roles !="freelancer")
                    <div class="col-md-3 ">
                        @include('layouts/sidebar') 
                    </div>
                    @endif
                    @if (auth()->user()->roles !="freelancer")
                    <div class="col-md-9 col-xs-12">
                    @else 
                    <div class="col-md-12 col-xs-12">
                    @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Profile</h4>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="{{ route('profile') }}/update/{{Auth::user()->id}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                                            @csrf
                                            {{ method_field('PUT') }}
                                            <div class="form-group row">
                                                <label for="email" class="col-4 col-form-label">Profile Image</label> 
                                                <div class="col-8">
                                                    <img src="{{ URL::asset('uploads/avatar') }}/{{ Auth::user()->avatar }}" id="avatar-img-box" style="display: block; width: 150px;height: 150px;background-color: gray;">

                                                    <input type="file" id="avatar" name="avatar" accept="image/*" onchange="document.getElementById('avatar-img-box').src = window.URL.createObjectURL(this.files[0])">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-4 col-form-label">User Name / Email <span class="text-danger">*</span></label> 
                                                <div class="col-8">
                                                    <input id="email" disabled value="{{ Auth::user()->email }}" placeholder="Email" class="form-control here" required="required" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="name" class="col-4 col-form-label">Name <span class="text-danger">*</span></label> 
                                                <div class="col-8">
                                                    <input id="name" name="name" value="{{ Auth::user()->name }}" required="required" placeholder="First Name" class="form-control here" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="publicName" class="col-4 col-form-label">Display Public Name <span class="text-danger">*</span></label> 
                                                <div class="col-8">
                                                    <input id="publicName" name="publicName" required="required" value="{{ Auth::user()->publicName }}" autocomplete="false" placeholder="Public Name" class="form-control here" type="text">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="publicInfo" class="col-4 col-form-label">Short Introduction</label> 
                                                <div class="col-8">
                                                    <textarea id="publicInfo" name="publicInfo" cols="40" rows="4" class="form-control">{{ Auth::user()->publicInfo }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="skills" class="col-4 col-form-label">Skills</label> 
                                                <div class="col-8">
                                                    @foreach (json_decode($skills[0]->skill) as $item)
                                                        <div class="custom-control custom-checkbox custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input" name="skill[]" id="{{$item}}" value="{{$item}}" @if (isset($userSkills[0]->skills) && !empty($userSkills[0]->skills)) @if (in_array($item, json_decode($userSkills[0]->skills))) checked  @endif @endif>
                                                            <label class="custom-control-label" for="{{$item}}">{{ucfirst($item)}}</label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="more_skills" class="col-4 col-form-label">Other Skills <small class="text-danger">(1 item per enter)</small></label>
                                                <div class="col-8">
                                                    <textarea name="more_skills" class="form-control" id="more_skills" cols="30" rows="4">@if (isset($userSkills[0]->other_skills) && !empty($userSkills[0]->other_skills))<?php
                                                        $custom_skills = json_decode($userSkills[0]->other_skills);
                                                        $cc_total = count($custom_skills);
                                                        $cc_counter = 0;
                                                        foreach ($custom_skills as $key => $value) { $cc_counter++;
                                                            echo $value;
                                                            if( $cc_counter < $cc_total ) {
                                                                echo "\r";
                                                            }
                                                        } ?>@endif</textarea>    
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="resume" class="col-4 col-form-label">Upload CV <small class="text-danger">(PDF Format)</small></label> 
                                                <div class="col-8">
                                                    <label for="resume" class="d-block text-primary"><i class="fa fa-file"></i> <a href="{{ URL::asset('uploads/cv') }}/{{ Auth::user()->cv }}" target="_blank">{{ Auth::user()->cv }}</a></label>
                                                    <input id="resume" name="resume" accept=".pdf" type="file">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="offset-4 col-8">
                                                    <button type="submit" class="btn btn-primary">Update Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                        <form action="{{ route('profile') }}/update/{{Auth::user()->id}}" method="POST">
                                            @csrf
                                            {{ method_field('PUT') }}
                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label">{{ __('New Password') }} <span class="text-danger">*</span></label>
            
                                                <div class="col-md-8">
                                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>
            
                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password-confirm" class="col-md-4 col-form-label">{{ __('Confirm New Password') }} <span class="text-danger">*</span></label>
            
                                                <div class="col-md-8">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="offset-4 col-8">
                                                    <button type="submit" class="btn btn-primary">Update Password</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection