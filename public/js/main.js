(function($) {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('new-job');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });

    }, false);

    // var loadFile = function(event) {
    //     var output = document.getElementById('vatar-img-box');
    //     output.src = URL.createObjectURL(event.target.files[0]);
    //     alert(event.target.files[0])
    // };
    // $(document).on("click","#avatar-img-box",function(){
    //     $("#avatar").trigger("click")
    // })

    // $("#avatar").change(function(){
    //     loadFile(this)
    // });

})(jQuery);