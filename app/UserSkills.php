<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkills extends Model
{
    protected $fillable = [
        'userid','skills','other_skills'
    ];
}
