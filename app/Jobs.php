<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $fillable = [
        'userid','title', 'description', 'categories','custom_categories','status','setas','max_bids','rate','expiry_date'
    ];

    
    public function applications() {
        return $this->hasMany('App\Applications', 'jobsId');
    }
}
