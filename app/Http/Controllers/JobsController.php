<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use Redirect;
use Session;
use DB;
use Auth;
use App\Jobs;
use App\Applications;
use Response;
use Config;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = \DB::table('jobs')
                ->leftJoin('applications', 'jobs.id', '=', 'applications.jobsId')
                ->select('jobs.*', 'applications.userID', 'applications.jobsId' , DB::raw('count(applications.jobsId) as applicationsCount'))
                ->groupBy("jobs.id")
                ->orderBy("jobs.updated_at", "desc")
                ->where("jobs.userid", auth()->user()->id)
                ->paginate(6);
        $arr = [
            "create"    => false,
            "title" => "Jobs",
            "jobs"  => $jobs
        ];
        return view("/jobs",$arr);
    }

    public function emailTest() {
        $name = 'Krunal';
        Mail::to('jumamoy.filjumar@gmail.com')->send(new SendMailable($name));
        
        return 'Email was sent';
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $skills = \DB::table('skills')->select("*")->orderBy("id", "asc")->get();
        $arr = [
                "create"    => true,
                "title" => "Create Job",
                "skills"    => $skills
                ];

        return view("/jobs",$arr);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $custom_category = $request->input('more_category');
        if(isset($custom_category) && !empty($custom_category)) {
            $cc = json_encode(explode("\r\n", $custom_category));
        }else{
            $cc = "";
        }
        $success = Jobs::create([
            'userid'  => auth()->user()->id,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'categories' => json_encode($request->input('category')),
            'custom_categories' => $cc,
            'status' => $request->input('status'),
            'setas' => $request->input('setas'),
            'max_bids' => $request->input('max_bids'),
            'rate' => $request->input('rate'),
            'expiry_date' => $request->input('exp_date')
        ]);

        if($success) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => " Job successfully posted.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('jobs')->with('message', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobs = \DB::table("jobs")
                ->leftJoin('applications', 'jobs.id', '=', 'applications.jobsId')
                ->leftJoin('users', 'applications.userID', '=', 'users.id')
                ->select('jobs.*','jobs.id as jID', 'applications.*','users.*')
                ->where('jobs.id',decrypt($id))
                ->get();

        $applicants = \DB::table("applications")
                    ->leftJoin('jobs', 'applications.jobsId', '=', 'jobs.id')
                    ->leftJoin('users', 'applications.userID', '=', 'users.id')
                    ->leftJoin('user_skills', 'users.id', '=', 'user_skills.userid')
                    ->select("applications.*","applications.id as appID","applications.status as appStatus","users.*","users.id as uid","user_skills.*","jobs.*")
                    ->where('applications.jobsId',decrypt($id))
                    ->get();

        $appStatus = 0;
        $stats = false;
        foreach ($applicants as $key => $value) {
            if($value->uid == auth()->user()->id) {
                $appStatus = $value->appStatus;
                if($appStatus > -1) {
                    $stats = true;
                }else{
                    $stats = false;
                }
            }
        }
        if(count($jobs)>0) {
            return \View::make('applications', 
                ['post_info' => $jobs, 
                'applicants' => $applicants,
                'appStatus' => $stats,
                "myapplications" => false
                ])->with('jobs', $jobs);
        }else{
            return \Response::view('errors.404',array(),404);
        }
    }

    /*
    *
    * Store new application data
    *
    *@param $id
    *@return \Illuminate\Http\Response
    */
    public function apply($id)
    {
        $email = auth()->user()->email;
        $success = Applications::create([
            'userId'  => auth()->user()->id,
            'jobsId' => decrypt($id)
        ]);

        if($success) {

            // notify admin if someone applied for the specific job post
            $msg = array("type" => "success", "title" => "Success!", "msg" => " You have successfully applied to this job.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('jobs/'.$id)->with('message', $msg);
    }

    /*
    *
    * Accept application data
    *
    *@param $id
    *@return \Illuminate\Http\Response
    */
    public function hire($id)
    {
        $success = Applications::where("id", decrypt($id))->update([
            'status' => 1,
            'hired_at' => date("Y-m-d h:i:s")
        ]);

        if($success) {
            $success_application = \DB::table("applications")
                                ->leftJoin('users', 'applications.userID', '=', 'users.id')
                                ->select()
                                ->where('applications.id',decrypt($id))
                                ->get();
            $msg = array("type" => "success", "title" => "Success!", "msg" => $success_application[0]->name." was successfully hired to this job.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        return Redirect::back()->with('message', $msg);
    }


    /*
    *
    * Decline application data
    *
    *@param $id
    *@return \Illuminate\Http\Response
    */
    public function decline($id)
    {
        $success = Applications::where("id", decrypt($id))->update([
            'status' => 2,
            'declined_at' => date("Y-m-d h:i:s")
        ]);

        if($success) {
            $success_application = \DB::table("applications")
                                ->leftJoin('users', 'applications.userID', '=', 'users.id')
                                ->select()
                                ->where('applications.id',decrypt($id))
                                ->get();
            $msg = array("type" => "success", "title" => "Success!", "msg" => $success_application[0]->name." was successfully declined.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return Redirect::back()->with('message', $msg);
    }

    /*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $id = decrypt($id);
        $jobs = Jobs::where('id',$id)->get();
        
        $skills = \DB::table('skills')->select("*")->orderBy("id", "asc")->get();

        if(count($jobs)>0) {
            $arr = [
                "create"    => true,
                "title" => "Update Job",
                "id"    => $id,
                "jobs"  => $jobs,
                "skills" => $skills
                ];
            return view("/jobs",$arr);
        }else{
            return \Response::view('errors.404',array(),404);
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $custom_category = $request->input('more_category');
        if(isset($custom_category) && !empty($custom_category)) {
            $cc = explode("\r\n", $custom_category);
        }else{
            $cc = "";
        }

        $success = Jobs::find(decrypt($id))->update(
        [
            'userid'  => auth()->user()->id,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'categories' => json_encode($request->input('category')),
            'custom_categories' => $cc,
            'setas' => $request->input('setas'),
            'status' => $request->input('status'),
            'max_bids' => $request->input('max_bids'),
            'rate' => $request->input('rate'),
            'expiry_date' => $request->input('exp_date')
        ]);

        if($success) {
            $msg = array("type" => "success", "title" => "Success!", "msg" => " Job successfully updated.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('jobs/edit/'.$id)->with('message', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);

        $jobs = Jobs::find($id);
        if($jobs->delete($id)) {
            
            Applications::where('jobsId',$id)->delete();

            $msg = array("type" => "success", "title" => "Success!", "msg" => " Data was successfully deleted.");
        }else{
            $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
        }
        
        return \Redirect::to('jobs')->with('message', $msg);
    }
}
