<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\UserSkills;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skills = \DB::table('skills')->select("*")->orderBy("id", "asc")->get();
        $user_skills = UserSkills::where('userid',auth()->user()->id)->get();

        $arr = [
            "skills"    => $skills,
            "userSkills"    => $user_skills
        ];
        return view("/profile", $arr);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $v = Validator::make($request->all(), [
            'password'  => 'nullable|required_with:password_confirmation|string|confirmed',
            'avatar'    =>  'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        } else {
            if( !empty($request->input('name')) ) {

                $custom_skill = $request->input('more_skills');
                if(isset($custom_skill) && !empty($custom_skill)) {
                    $cc = json_encode(explode("\r\n", $custom_skill));
                }else{
                    $cc = "";
                }

                $skill_exist = UserSkills::where('userid',auth()->user()->id)->get();
                if(count($skill_exist) > 0) {
                    UserSkills::where("userid",auth()->user()->id)->update(
                        [
                            "skills"  =>  json_encode($request->input('skill')),
                            "other_skills"   => $cc
                    ]);
                }else{
                    UserSkills::where("userid",auth()->user()->id)->create(
                        [
                            "userid"    => auth()->user()->id,
                            "skills"  =>  json_encode($request->input('skill')),
                            "other_skills"   => $cc
                    ]);
                }
                
                $resume = $request->file('resume');
                $resumeUrl = "";

                if(!empty($resume)) {
                    $resumePath = 'uploads/cv/';
                    $resumeUrl = auth()->user()->id."_".date('mdY').".".$resume->getClientOriginalExtension();
                    $resume->move($resumePath, $resumeUrl);
                    User::find($id)->update(
                    [
                        'cv'  => $resumeUrl
                    ]);
                }

                $files = $request->file('avatar');
                $profileImage = "";
                
                if (!empty($files)) {
                    $destinationPath = 'uploads/avatar/'; // upload path
                    $profileImage = auth()->user()->id.'_'.date('mdY') . "." . $files->getClientOriginalExtension();
                    $files->move($destinationPath, $profileImage);
                    $success = User::find($id)->update(
                    [
                        'name'  => $request->input('name'),
                        'publicName'  => $request->input('publicName'),
                        'publicInfo'  => $request->input('publicInfo'),
                        'avatar'  => $profileImage
                    ]);

                }else{
                    
                $success = User::find($id)->update(
                    [
                        'name'  => $request->input('name'),
                        'publicName'  => $request->input('publicName'),
                        'publicInfo'  => $request->input('publicInfo')
                    ]);
                }


            }else{
                $success = User::find($id)->update(
                [
                    'password' => $request->input('password')
                ]);
            }
                
            if($success) {
                $msg = array("type" => "success", "title" => "Success!", "msg" => " Job successfully updated.");
            }else{
                $msg = array("type" => "danger", "title" => "Error!", "msg" => " Something went wrong. Please try again later.");
            }
            return \Redirect::to('profile')->with('message', $msg);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
