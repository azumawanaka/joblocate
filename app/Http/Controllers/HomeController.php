<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Session;
use DB;
use Auth;
use App\Jobs;
use App\Applications;
use Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if(!empty($request->input("search"))) {
            $jobs = \DB::table('jobs')
                ->leftJoin('applications', 'jobs.id', '=', 'applications.jobsId')
                ->leftJoin('users', 'jobs.userid', '=', 'users.id')
                ->select('users.name','users.publicName','users.publicInfo','jobs.*', 'applications.jobsId','applications.userID' , DB::raw('count(applications.jobsId) as applicationsCount'))
                ->groupBy("jobs.id")
                ->orderBy("jobs.updated_at", "desc")
                ->where("jobs.title", 'LIKE', '%' .$request->input("search").'%')
                ->paginate(6);
        }else{
            $jobs = \DB::table('jobs')
                    ->leftJoin('applications', 'jobs.id', '=', 'applications.jobsId')
                    ->leftJoin('users', 'jobs.userid', '=', 'users.id')
                    ->select('users.name','users.publicName','users.publicInfo','jobs.*', 'applications.jobsId','applications.userID' , DB::raw('count(applications.jobsId) as applicationsCount'))
                    ->groupBy("jobs.id")
                    ->orderBy("jobs.updated_at", "desc")
                    ->paginate(6);
        }
        $arr = [
            "create"    => false,
            "title" => "Jobs",
            "jobs"  => $jobs
        ];
        return view("home",$arr);
    }
}
