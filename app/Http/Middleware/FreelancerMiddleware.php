<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class FreelancerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(!Auth::guest()) {
            if (auth()->user()->roles != 'client')
            {
                return \Redirect::to('/unauthorized');
            }else{
                return $next($request);
            }
        }else{
            return \Redirect::to('/');
        }
    }
}
