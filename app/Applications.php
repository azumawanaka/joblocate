<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    protected $fillable = [
        'jobsId', 'userId','status','hired_at','declined_at'
    ];
}
